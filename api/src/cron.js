const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedulesServices");

// Agendamento da limpeza
cron.schedule("0 0 * * *", async () => {
    try {
        await cleanUpSchedules();
        console.log("lipeza automatica");
    } catch (error) {
        console.error("Erro ao executar limpeza automatica");
    }
});